#/bin/sh
# Cleanup /var/log directory.
sudo truncate /var/log/boot.log --size 0
sudo truncate /var/log/btmp --size 0
sudo truncate /var/log/cloud-init.log --size 0
sudo truncate /var/log/cloud-init-output.log --size 0
sudo truncate /var/log/cron --size 0
sudo truncate /var/log/dmesg --size 0
sudo truncate /var/log/dmesg.old --size 0
sudo truncate /var/log/dracut.log --size 0
sudo truncate /var/log/lastlog --size 0
sudo truncate /var/log/maillog --size 0
sudo truncate /var/log/messages --size 0
sudo truncate /var/log/ntp.log  --size 0
sudo truncate /var/log/secure --size 0
sudo truncate /var/log/spooler --size 0
sudo truncate /var/log/tallylog --size 0
sudo truncate /var/log/wtmp --size 0
sudo truncate /var/log/yum.log --size 0
