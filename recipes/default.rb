#
# Cookbook:: ops-base
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

package 'htop' do
  action :install
end

package 'httpd' do
  action :install
end

package 'screen' do
  action :install
end

package 'tmux' do
  action :install
end

package 'sysstat' do
  action :install
end

package 'psacct' do
  action :install
end

service 'psacct' do
  action [:enable, :start]
end

package 'ntp' do
  action :install
end

package 'traceroute' do
  action :install
end

package 'telnet' do
  action :install
end

cookbook_file '/etc/ntp.conf' do
  source 'ntp.conf'
  owner 'root'
  group 'root'
  mode  '0644'
  action :create
end

service 'ntpd' do
  action [:enable, :start]
end

service 'sshd' do
  action [:enable, :start]
end

service 'network' do
  action [:enable, :start]
end

# SSH Configuration.
cookbook_file '/etc/ssh/sshd_config' do
  source 'sshd_config'
  owner 'root'
  group 'root'
  mode  '0644'
  action :create
end

# Banner creation.
cookbook_file '/etc/motd' do
  source 'motd'
  owner 'root'
  group 'root'
  mode  '0644'
  action :create
end

service 'sshd' do
  action :restart
end

file '/var/log/wtmp' do
  owner 'root'
  group 'utmp'
  mode '664'
  action :create
end

group 'admin' do
end

user 'admin' do
  password '$1$tata$.y/rEeq60zUAVqnFPaSiZ/'
  group 'admin'
  shell '/bin/bash'
  home '/home/admin'
  manage_home true
  action :create
end

execute 'Admin Rights' do
  command 'usermod -a -G wheel admin'
  user 'root'
end

directory '/home/admin/.ssh' do
  action :create
end

execute 'copy ssh file' do
  command 'cp -rvp /home/ec2-user/.ssh/authorized_keys /home/admin/.ssh/authorized_keys'
  user 'root'
  action :run
end

file '/home/admin/.ssh/authorized_keys' do
  owner 'admin'
  group 'admin'
  mode '600'
  action :create
end

execute 'Replace ec2 sudo user' do
  command 'echo "admin ALL = NOPASSWD: ALL
 # User rules for ec2-user
 admin ALL=(ALL) NOPASSWD:ALL
 # User rules for ec2-user
 admin ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/cloud-init'
  user 'root'
end

cookbook_file '/tmp/var_log.sh' do
  source 'var_log.sh'
  owner 'root'
  group 'root'
  mode  '0755'
  action :create
end

execute 'exicute var_log.sh script' do
  command '/tmp/./var_log.sh'
  user 'root'
end

set_limit 'root' do
  type 'hard'
  item 'nofile'
  value 65536
  use_system true
end

set_limit 'root' do
  type 'soft'
  item 'nofile'
  value 65536
  use_system true
end

# System-wide limits
set_limit '*' do
  type 'hard'
  item 'nofile'
  value 65536
  use_system true
end

sysctl_param 'net.ipv4.tcp_syncookies' do
  value 1
  action :apply
end

sysctl_param 'net.ipv4.conf.all.accept_source_route' do
  value 0
  action :apply
end

sysctl_param 'net.ipv4.conf.all.accept_redirects' do
  value 0
  action :apply
end

sysctl_param 'net.ipv4.conf.all.rp_filter' do
  value 1
  action :apply
end

sysctl_param 'net.ipv4.icmp_echo_ignore_broadcasts' do
  value 1
  action :apply
end

sysctl_param 'net.ipv4.conf.all.log_martians' do
  value 1
  action :apply
end

selinux_state "SELinux Disabled" do
  action :disabled
end

execute 'remove .bash_history' do
  command 'rm -rf ~/.bash_history'
  user 'root'
end

execute 'Remote data inside of /tmp' do
  command 'rm -rf /tmp/*'
  user 'root'
end

